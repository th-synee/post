<html>
<head>
    <title>Post</title>
    <link rel="stylesheet" href="/static/app/style/style.css">
    <script src="/static/lib/jquery-2.0.3.js"></script>
    <script src="/static/lib/underscore.js"></script>
    <script src="/static/lib/backbone.js"></script>
    <script src="/static/lib/require.js" data-main="/static/app/main"></script>
</head>
<body>
    <div id="header">
        <span data-page="dashboard">
            <a href="#dashboard">Dashboard</a>
        </span>
        <span data-page="feed">
            <a href="#feed">Feed</a>
        </span>
        <span data-page="post">
            <a href="#post">Post</a>
        </span>
    </div>
    <section class="main" id="dashboard_main">
        <div class="tag-selector">
            <div class="tag-list">
                <span class="tag">xtime</span>
                <span class="tag">div</span>
                <span class="tag">html</span>
                <span class="tag">code</span>
                <span class="tag">mine</span>
                <span class="tag">me</span>
            </div>
            <div class="tag-input">
                <span class="sort drop-menu">
                    排序
                    <ul class="selector">
                        <li data-type="time" class="selectable">时间</li>
                        <li data-type="type" class="selectable">类型</li>
                        <li data-type="name" class="selectable">名称</li>
                    </ul>
                </span>
                <input>
            </div>
        </div>
        <aside class="side-bar">
            <ul class="selector">
                <li class="action" class="selectable">
                    action No.1
                </li>
                <li class="action selectable">
                    action No.2
                </li>
                <li class="action selectable">
                    action No.3
                </li>
                <li class="action selectable">
                    action No.4
                </li>
            </ul>
        </aside>
        <div class="object-container">
            <div class="object video"></div>
            <div class="object png"></div>
            <div class="object txt"></div>
            <div class="object gif"></div>
            <div class="object js"></div>
            <div class="object png"></div>
            <div class="object html"></div>
            <div class="object video"></div>
            <div class="object gif"></div>
            <div class="object png"></div>
            <div class="object txt"></div>
            <div class="object gif"></div>
            <div class="object js"></div>
            <div class="object png"></div>
            <div class="object html"></div>
            <div class="object"></div>
            <div class="object js"></div>
            <div class="object png"></div>
            <div class="object html"></div>
            <div class="object video"></div>
            <div class="object png"></div>
            <div class="object txt"></div>
            <div class="object gif"></div>
            <div class="object js"></div>
            <div class="object png"></div>
            <div class="object html"></div>
            <div class="object"></div>
            <div class="object js"></div>
            <div class="object png"></div>
            <div class="object html"></div>
            <div class="object"></div>
            <div class="object js"></div>
            <div class="object png"></div>
            <div class="object html"></div>
            <div class="object"></div>
        </div>
    </section>
    <section class="main" id="feed_main"></section>
    <section class="main" id="post_main">
        <ul class="post-list"></ul>
    </section>
</body>
</html>