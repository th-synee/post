define (require)->

    Model = require "attach/model/AttachModel"

    Backbone.View.extend

        initialize: (options)->
            @model = new Model({id: options.id})
            @model.on("change", @render, @)
            @model.fetch()


        render: ->
            if @model.get("type") in ["image/png", "image/gif", "image/jpeg"]
                @$el.html($("<img src='#{@model.get("url")}'>"))
            @