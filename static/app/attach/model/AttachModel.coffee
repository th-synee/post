define (require)->


    UserModel = require "user/model/UserModel"

    Backbone.Model.extend
        initialize: ->

        fetch: (callback=()->)->
            self = @
            if not @id
                return
            $.get("/attach/#{@id}", (response)->
                callback(response)
#                console.log(response)
                self.set(response)
            )
