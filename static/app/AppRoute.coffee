define  (require) ->
    Backbone.Router.extend

        initialize: ->
            @model = new Backbone.Model({main: undefined });
            @model.on("change:main", @_swithmain, @)
            @mainMap = {
                dashboard: "dashboard/view/MainView"
                post: "post/view/MainView"
            }

        routes:
            ":main": "_main"


        _main: (main)->
            if @mainMap[main]
                @model.set({main: main})

        _home: ()->
            console.log("home")

        _swithmain: ()->
            self = @
            $(".main[id]").hide()
            if(@[@model.get("main")])
                @[@model.get("main")].render()
                @[@model.get("main")].$el.show();
                @[@model.get("main")].$el.trigger("show")
                return
            app.runview(@mainMap[@model.get("main")], {el: "##{@model.get("main")}_main"}, (view)->
                self[self.model.get("main")] = view
                view.$el.show();
                view.$el.trigger("show")
            )

