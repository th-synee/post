define (require)->
    require "Handlebars"
    AppRoute = require "AppRoute"
    AppView = require "AppView"
    class Application

        init: ()->

            new AppView
            new AppRoute

            Backbone.history.start()

            @store = {attach: {}}

        runview: (path, attr, callBack = ->)->
            requirejs([path], (View)->
                view = new View(attr).render()
                callBack(view)
            )



