define (require) ->
    Backbone.View.extend

        events: {
            "click aside li.action": "_action"
        }

        _action: (e)->
            @$("aside li.action").removeClass("selected")
            $(e.target).addClass("selected")

        initialize: ->
            self = @
            $(window).on("resize", ->
                self._resize()
            )
            @$el.on("show", ->
                self._resize()
            )

        _resize: ->
            $container = $(".object-container")
            $container.height(window.innerHeight - $container.offset().top - @$(".tag-selector").height())

        render: ()->
            @