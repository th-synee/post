requirejs.config
    baseUrl: "/static/app"
    paths:
        text: "/static/lib/text"
        Handlebars: "/static/lib/handlebars-v1.1.2"

requirejs ['Application'], (Application)->
    window.app = new Application()
    app.init()
