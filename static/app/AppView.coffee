define (require)->

    Backbone.View.extend
        el: "body"

        events: {
            "click .selector .selectable": "_select"
        }

        _select: (e)->
            $selector = $(e.currentTarget).closest(".selector")
            $selector.find(".selectable").removeClass("selected")
            $(e.currentTarget).addClass("selected")
            $selector.trigger("selected", [e, $(e.currentTarget)])

        initialize: ->
