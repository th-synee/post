define (require)->


    UserModel = require "user/model/UserModel"

    Backbone.Model.extend

        initialize: ->
            @getAuthor()

        getAuthor: ()->
            if not @get("author_id")
                return null

            if not @author
                if @get("author")
                    @author = new UserModel(@get("author"))
                else
                    @author = new UserModel({id: @get("author_id")})

            return @author

        create: (done=(->), fail=undefined )->
            jqxhr = $.post("/post/create", @attributes);
            jqxhr.done(done)

