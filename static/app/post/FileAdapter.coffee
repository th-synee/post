define (require)->

    # save File to server
    sendFile = (file, callback=(->))->
        formData = new FormData();
        formData.append("file", file)

        # ajax
        xhr = new XMLHttpRequest();
        xhr.open('POST', '/attach/save');
        xhr.onreadystatechange = ->
            if (xhr.readyState==4 && xhr.status==200)
                callback(JSON.parse(xhr.response))
        xhr.send(formData);


    abstractAdapter = (file, $attachBox, callBack=(->), type, readmethod="readAsArrayBuffer") ->
        reader = new FileReader()
        $attach = undefined
        reader.onloadend = (e) ->
            $attach = $("""<span
                        data-name="#{file.name}"
                        data-type="#{file.type}"
                        data-size="#{file.size}"
                        class='attach-one #{type}'></span>""")
            sendFile(file, (response) ->
                $attach.attr({"data-id": response.id});
            )
            callBack(e, $attach)
            $attachBox.html($attach)
            $attachBox.removeClass("none-attach")
            return false
        reader[readmethod](file)



    imgAdapter = (file, $attachBox)->
        abstractAdapter(file, $attachBox, (e, $attach)->
            $attach.html($("<img src='#{e.target.result}'>"))
        , "image", "readAsDataURL")

    textAdapter = (file, $attachBox)->
        abstractAdapter(file, $attachBox, (e, $attach)->
            $attach.text(file.type or "text")
        , "text", "readAsText")

    adapter = {}

    # image adapter
    adapter[type] = imgAdapter for type in ["image/png", "image/gif", "image/jpeg"]

    # text adapter
    adapter[type] = textAdapter for type in ["text/javascript", "text/plain"]


    return adapter