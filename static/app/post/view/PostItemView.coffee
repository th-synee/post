define (require)->

    require "Handlebars"
    template = require "text!post/template/post_item.handlebars"

    Backbone.View.extend

        events:
            "click .text": "_test"
            "click .delete": "_delete"
            "click .action-bar .action": (e)->@["""_#{$(e.target).data("type")}"""]()

        template: Handlebars.compile(template)

        _test: (e)->
#            console.log e

        _delete: (e) ->
            self = @
            jqxhr = $.post("/post/delete", {id: @model.id})
            jqxhr.done((response)->
                if(response.success)
                    self.trigger("delete")
            )
            jqxhr.fail(()->
            )

        _like: ()->
            console.log("like")
            @$(".action-box").fadeOut()

        _comment: ()->
            console.log("comment")
            $comment = $("<div class='comment-input'><textarea></textarea><span>取消</span></div>")
            @$(".action-box").html($comment)
            @$(".action-box").fadeIn()

        _collect: ()->
            console.log("collect")
            @$(".action-box").fadeOut()


        _renderTags: ()->
            self = @
            jqxhr = $.get("/post/tags", {id: @model.id})
            jqxhr.done((response)->
                _.each response, (tag, index)->
                    self.$(".tags").append($("<span data-type='tag' date-id='#{tag.id}'>#{tag.name}</span>"))
            )
            jqxhr.fail(()->
                console.log("------------------------")
                console.log(arguments)
                console.log(self.model)
            )

        _renderAttach: ()->
            app.runview("attach/view/AttachView", {id: @model.get("attach_id"), el: @$(".attach")})



        _renderComments: ()->


        initialize: ->


        render: ->
            $html = $(@template(@model.toJSON()))
            @setElement($html)
            @_renderTags()
            @_renderAttach()
            @_renderComments()
            @

