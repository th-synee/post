define (require) ->

    PostInputView = require("post/view/PostInputView")
    PostListView = require("post/view/PostListView")
    Collection = require "post/collection/PostCollection"

    Backbone.View.extend

        el: "#post_main"

        initialize: ()->
            self = @
            @collection = new Collection()
            @newCollection = new Collection()
            @inputModel = new @collection.model

            @postInputView = new PostInputView
                model: @inputModel
                collection: @collection
                newCollection: @newCollection
            .render()

            @$el.prepend(@postInputView.$el)

            @postListView = new PostListView
                el: @$(".post-list")
                collection: @collection
                newCollection: @newCollection
            .render()

            $(window).on("resize", ->
                self._resize()
            )
            @$el.on("show", ->
                self._resize()
            )

        _resize: ->
            $container = @$(".post-list")
            $container.height(window.innerHeight - $container.offset().top)


        appendPost: (model)->
            @collection.create(model)


        render: ()->
            @$el.show()
            @







