define (require)->

    Model = require "post/model/PostModel"

    Backbone.Collection.extend

        url: "/post/list"

        model: Model

        initialize: ->
            @pageCount = 1

        nextPage: ()->
            self = @
            @pageCount++
            @query.page = @pageCount
            jqxhr = $.get(@url, @query)
            jqxhr.done((response)->
                if response.length == 0
                    self.pageCount--
                console.log(self.pageCount)
                self.add(response)
            )
            jqxhr.fail(()->
                console.log(arguments)
            )


        fetch: (query={}) ->
            self = @
            @query = query
            jqxhr = $.get(@url, query)
            jqxhr.done((response)->
                self.reset(response)
                @pageCount = 1
            )
            jqxhr.fail(()->
                console.log(arguments)
            )