<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="/static/account/style/style.css">
    <script src="/static/lib/jquery-2.0.3.js"></script>
</head>
<body>
    <div id="form_container">
        <form method="post" id="login_form">
            <h2>Tag xTime</h2>
            <label>
                <input type="text" name="account" placeholder="邮箱/用户名/手机号">
            </label>
            <label>
                <input type="password" name="password" placeholder="密码">
            </label>
            <label>
                <button type="submit">登<span class="space"></span>陆</button>
                <a class="another" href="/register">新建账号</a>
            </label>
        </form>
        <script>
            $("button").click(function(){
                if(!$("input[name='account']").val() || !$("input[name='password']").val()){
                    return alert("请将信息填写完整");
                }
            })
        </script>
    </div>
</body>
</html>