<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="/static/account/style/style.css">
    <style type="text/css">
        .nav{
            padding: 4px;
        }
        .nav div{
            width: 32%;
            float: left;
            background: #aaa;
            color: #fff;
            text-align: center;
            padding: 5px;
            margin: 1px;
            box-sizing: border-box;
            cursor: pointer;
        }
        .nav div.selected{
            background: #fff;
            color: #333;
        }
        #form_container input.warn{
            border-color: #c66;
        }
    </style>
    <script src="/static/lib/jquery-2.0.3.js"></script>
</head>
<body>
    <div id="form_container">
        <h2>Tag xTime</h2>
        <div class="nav">
            <div data-target="form_1" class="selected">邮箱登陆</div>
            <div data-target="form_2">用户名登陆</div>
            <div data-target="form_3">手机号登陆</div>
        </div>
        <form style="display: block" method="post"class="form_1">
            
            <label>
                <input type="text" name="email" placeholder="邮箱">
            </label>
            <label>
                <input type="password" name="password" placeholder="密码">
            </label>
            <label>
                <input type="password" name="repasswd" placeholder="确定密码">
            </label>
            <label>
                <button type="submit" disabled="disabled">注<span class="space"></span>册</button>
                <a class="another" href="/login">已有账号</a>
            </label>
        </form>

        <form style="display: none" method="post"class="form_2">
            
            <label>
                <input type="text" name="username" placeholder="用户名">
            </label>
            <label>
                <input type="password" name="password" placeholder="密码">
            </label>
            <label>
                <input type="password" name="repasswd" placeholder="确定密码">
            </label>
            <label>
                <button type="submit" disabled="disabled">注<span class="space"></span>册</button>
                <a class="another" href="/login">已有账号</a>
            </label>
        </form>

        <form style="display: none" method="post"class="form_3">
            
            <label>
                <input type="text" name="number" placeholder="手机号">
            </label>
            <label>
                <input type="password" name="password" placeholder="密码">
            </label>
            <label>
                <input type="password" name="repasswd" placeholder="确定密码">
            </label>
            <label>
                <button type="submit" disabled="disabled">注<span class="space"></span>册</button>
                <a class="another" href="/login">已有账号</a>
            </label>
        </form>
    </div>
    <script>
        $(document).ready(function(){
            $(".nav div").click(function(e){
                $(".nav div").removeClass("selected");
                $(e.target).addClass("selected");
                $("#form_container form").hide();
                $("#form_container form." + $(e.target).data("target")).show();
            });

            $("input[name='repasswd'],input[name='password']").keyup(function(e){
                $form = $(e.target).closest("form");
                if($form.find("input[name='repasswd']").val() != $form.find("input[name='password']").val()){
                    $form.find("input[name='repasswd']").addClass("warn");
                    $form.find("button").attr({disabled: "disabled"});
                }else{
                    $form.find("input[name='repasswd']").removeClass("warn");
                    if($($form.find("input")[0]).val())
                        $form.find("button").removeAttr("disabled");
                }
            });
            $($("input")[0]).keyup(function(e){
                $form = $(e.target).closest("form");
                if(!$form.find("input[name='repasswd']").val() || !$(e.target).val()){
                    $form.find("button").attr({disabled: "disabled"});
                }else{
                    $form.find("button").removeAttr("disabled");
                }
            });
        });
    </script>
</body>
</html>